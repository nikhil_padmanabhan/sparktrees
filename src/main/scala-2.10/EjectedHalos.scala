/**
 * Created by npadmana on 6/20/15.
 *
 * The goal here is to actually compute the ejected halos.
 *
 * For completeness, here is the header for the trees...
 *
 * #scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8)
 * sam_mvir(9) mvir(10) rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19)
 * vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28)
 * Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32)
 * Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Rs_Klypin Mvir_all M200b M200c
 * M500c M2500c Xoff Voff Spin_Bullock b_to_a c_to_a A[x] A[y] A[z] b_to_a(500c) c_to_a(500c)
 * A[x](500c) A[y](500c) A[z](500c) T/|U| M_pe_Behroozi M_pe_Diemer
 *
 */

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf


// Start by building up a structure for the halos; we'll keep the key
// tags here only.

case class Halo(Id : Long, // ID
                DescId : Long, // ID of descendant halo
                UPId : Long, // ID of most massive host halo
                TreeRootID : Long, // ID of halo at tree root
                MMP : Int, // ==1, then is most massive progenitor.
                Scale : Float, // scale factor
                Mvir : Float, // Mass
                x : Float,
                y : Float,
                z : Float
                 )


object EjectedHalos {

  // Simple wrapper
  def mkHalo(line: String): Halo = {
    val l = line.trim.split("\\s+")
    new Halo(
      l(1).toLong,
      l(3).toLong,
      l(6).toLong,
      l(29).toLong,
      l(14).toInt,
      l(0).toFloat,
      l(10).toFloat,
      l(17).toFloat,
      l(18).toFloat,
      l(19).toFloat
    )
  }

  // Strip out comments and other such stuff
  // The length removes the first line which is just the number
  // of trees in the file. It's a hack.
  def isHalo(l: String): Boolean = {
    val l1 = l.trim()
    !(l1.startsWith("#") || (l1.length < 20))
  }

  // Clean out spurious halos in the main trunk
  // this ensures that each consecutive halo is a direct progenitor  of the
  // last.
  // i.e ID(i) == DescID(i+1)
  def cleanTrunk(tt : Array[Halo]) : Array[Halo] = {
    var prevId = tt(0).Id
    var ll = tt(0) :: Nil
    val nHalo = tt.length
    var ii = 1
    while (ii < nHalo) {
      if (tt(ii).DescId == prevId) {
        ll = tt(ii) :: ll
        prevId = tt(ii).Id
      }
      ii += 1
    }
    ll.reverse.toArray
  }

  // A trivial ejection criterion
  def isEjected(h : Array[Halo]): Byte = {
    if (h.exists(_.UPId != -1)) 1 else 0
  }


  def getEjectedHalos(sc : SparkContext) = {
    val halos = sc.textFile("tree_3_3_0_small.dat.gz")
      .filter(isHalo(_))
      .map(mkHalo(_))

    // Work on extracting the main trunk
    // The last sort makes sure that we have halos in descending scale factor order
    val trunk = halos.filter(_.MMP==1)
      .map(h => (h.TreeRootID, h))
      .groupByKey()
      .map({case (k,v) => (k,v.toArray.sortWith(_.Scale > _.Scale))})
      .map({case (k,v) => cleanTrunk(v)})

    // Consider host halos, and add an ejection tag
    val hosts = trunk.filter(_(0).UPId == -1) // Get hosts
      .map(h => s"${h(0).Id}  ${h(0).Mvir} ${isEjected(h)}") // Write in ejected tags

    hosts
  }

  // The main routine
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("EjectedHalos")
      .setMaster(args(0))
    val sc = new SparkContext(conf)

    val eject = getEjectedHalos(sc)

    eject.take(50).foreach(println)
    System.exit(0)

  }

}
