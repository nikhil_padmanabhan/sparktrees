/**
 * Created by npadmana on 6/19/15.
 *
 * This is my first foray int this, just demonstrating that I can successfully
 * parse these files...
 *
 * #scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8)
 * sam_mvir(9) mvir(10) rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19)
 * vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28)
 * Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32)
 * Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Rs_Klypin Mvir_all M200b M200c
 * M500c M2500c Xoff Voff Spin_Bullock b_to_a c_to_a A[x] A[y] A[z] b_to_a(500c) c_to_a(500c)
 * A[x](500c) A[y](500c) A[z](500c) T/|U| M_pe_Behroozi M_pe_Diemer
 *
 */

package Test {

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

case class Halo(Id: Long, DescId: Long, UPId: Long, TreeRootID: Long, MMP: Int,
                   Scale: Float)

// A very simple first attempt to parse the Bolshoi trees
object Parse1 {
  def isHalo(l: String): Boolean = {
    val l1 = l.trim()
    // The length removes the first line which is just the number
    // of trees in the file. It's a hack.
    !(l1.startsWith("#") || (l1.length < 20))
  }

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Parse1")
      .setMaster(args(0))
    val sc = new SparkContext(conf)
    // Build halo catalog
    val halos = sc.textFile("tree_3_3_0_small.dat.gz")
      .filter(isHalo(_))
      .map(l => l.trim.split("\\s+"))
      .map(l => Halo(l(1).toLong, l(3).toLong,
      l(6).toLong, l(29).toLong,
      l(14).toInt, l(0).toFloat))
    // Get the main trunk
    val trunk = halos.filter(h => h.MMP == 1)
      .map(h => (h.TreeRootID, h))
      .groupByKey
      .map({ case (k, v) => (k, v.toArray.sortWith(_.Scale < _.Scale)) })
    trunk.take(2).foreach({ case (k, v) => {
      print(k); v.foreach(print); println
    }
    })
    System.exit(0)
  }
}

}
