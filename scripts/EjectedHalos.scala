/**
 *
 * The next version of the EjectedHalos
 *
 * The goal is to output :
 *   <Id>  <Mass> <x> <y> <z> <HostId> <HostMass> <xHost> <yHost> <zHost>
 *
 * If ejected is 0, then HostID etc are set to the Id, Mass, etc
 *
 * Just for simplicity, I'll restrict to halos > 1e11
 *
 * Created by npadmana on 6/20/15.
 */

import sqlContext.implicits._
import org.apache.spark.sql._


// Define some basic classes
case class Halo(Id : Long, // ID
                DescId : Long, // ID of descendant halo
                UPId : Long, // ID of most massive host halo
                TreeRootID : Long, // ID of halo at tree root
                Scale : Float, // scale factor
                Mvir : Float, // Mass
                x : Float,
                y : Float,
                z : Float
                 )

// Simple wrapper
def mkHalo(r : Row): Halo = {
  new Halo(
    r.getLong(r.fieldIndex("Id")),
    r.getLong(r.fieldIndex("DescId")),
    r.getLong(r.fieldIndex("UPId")),
    r.getLong(r.fieldIndex("TreeRootID")),
    r.getFloat(r.fieldIndex("Scale")),
    r.getFloat(r.fieldIndex("Mvir")),
    r.getFloat(r.fieldIndex("x")),
    r.getFloat(r.fieldIndex("y")),
    r.getFloat(r.fieldIndex("z"))
  )
}

// Clean out spurious halos in the main trunk
// this ensures that each consecutive halo is a direct progenitor  of the
// last.
// i.e ID(i) == DescID(i+1)
def cleanTrunk(tt : Array[Halo]) : Array[Halo] = {
  var prevId = tt(0).Id
  var ll = tt(0) :: Nil
  val nHalo = tt.length
  var ii = 1
  while (ii < nHalo) {
    if (tt(ii).DescId == prevId) {
      ll = tt(ii) :: ll
      prevId = tt(ii).Id
    }
    ii += 1
  }
  ll.reverse.toArray
}

// A trivial ejection criterion
def isEjected(h : Array[Halo]): Long = {
  val myrootid = h(0).TreeRootID
  val h1 = h.find(_.UPId != -1)
  h1 match {
    case Some(n) => n.UPId
    case None => myrootid
  }
}

def mkString(h : Halo) : String = {
  s"${h.Id} ${h.Mvir} ${h.x} ${h.y} ${h.z}"
}

val trees = sqlContext.read.parquet("cacheTrees")

// for fun, work out the maximum scale factor
val maxscale = trees.agg(max($"Scale")).collect()(0).getFloat(0)-1.e-6
println(s"Using a maximum scale factor of $maxscale")

// Start building look up tables
//  0. ID --> TreeRootID
val Id2TreeRootId = trees.
  select("Id","TreeRootID").
  map(r => (r.getLong(r.fieldIndex("Id")),r.getLong(r.fieldIndex("TreeRootID")))).
  cache()

//  1. At z=0, Id -> UPId (If UPid==-1, set UPId=Id)
val Id2UPId = trees.filter(trees("Scale") > maxscale).
  select("Id","UPId").
  map(r => (r.getLong(r.fieldIndex("Id")),r.getLong(r.fieldIndex("UPId")))).
  map({case (k,v) => {if (v==(-1)) (k,k) else (k,v)}}).cache()

// 2. At z=0,
val halosToday = trees.filter(trees("Scale") > maxscale).
  map(mkHalo(_)).
  map(h => (h.TreeRootID, h)).cache()

// Now start building up ejected halos
val mmp = trees.filter(trees("MMP")===1)

val trunk = mmp.map(mkHalo(_)).
  map(h => (h.TreeRootID, h)).
  groupByKey().
  map({case (k,v) => (k,v.toArray.sortWith(_.Scale > _.Scale))}).
  map({case (k,v) => cleanTrunk(v)})

// Trim to Mvir > 1.e11 and only host halos
// note that hosts, still has the full tree
val hosts = trunk.filter(h => ((h(0).UPId == -1) && (h(0).Mvir > 1.e11)))

// Do it all
val eject = hosts.map(h => (isEjected(h),h(0))).  // (k,v) => (Host ID, z=0 halo)
  join(Id2TreeRootId). // Get TreeRootId
  map({case (k, (v,w)) => (w,v)}). // Clean up
  join(Id2UPId). // If the z=0 halo is a subhalo, go one up
  map({case (k, (v,w)) => (w,v)}). // Clean up
  join(halosToday). // Get today's info
  map({case (k, (v,w)) => (v,w)}). // Clean up
  map({case (v,w) => mkString(v)+" "+mkString(w)}). // Write in ejected tags
  saveAsTextFile("EjectedHosts")


System.exit(0)