/**
 * Created by npadmana on 6/20/15.
 *
 * This builds a very simple ejected halo file, after parsing the Bolshoi
 * trees. It's designed to be standalone...
 *
 * The output is halo ID, mass, and ejected?
 *    where ejected==1 if ejected
 *
 */

import sqlContext.implicits._
import org.apache.spark.sql._

// Note this isn't everything we've cached, but that's the nice part of this
case class Halo(Id : Long, // ID
                DescId : Long, // ID of descendant halo
                UPId : Long, // ID of most massive host halo
                TreeRootID : Long, // ID of halo at tree root
                Scale : Float, // scale factor
                Mvir : Float // Mass
                 )


// Simple wrapper
def mkHalo(r : Row): Halo = {
  new Halo(
    r.getLong(r.fieldIndex("Id")),
    r.getLong(r.fieldIndex("DescId")),
    r.getLong(r.fieldIndex("UPId")),
    r.getLong(r.fieldIndex("TreeRootID")),
    r.getFloat(r.fieldIndex("Scale")),
    r.getFloat(r.fieldIndex("Mvir"))
  )
}

// Clean out spurious halos in the main trunk
// this ensures that each consecutive halo is a direct progenitor  of the
// last.
// i.e ID(i) == DescID(i+1)
def cleanTrunk(tt : Array[Halo]) : Array[Halo] = {
  var prevId = tt(0).Id
  var ll = tt(0) :: Nil
  val nHalo = tt.length
  var ii = 1
  while (ii < nHalo) {
    if (tt(ii).DescId == prevId) {
      ll = tt(ii) :: ll
      prevId = tt(ii).Id
    }
    ii += 1
  }
  ll.reverse.toArray
}

// A trivial ejection criterion
def isEjected(h : Array[Halo]): Byte = {
  if (h.exists(_.UPId != -1)) 1 else 0
}

// Read in, and just toss anything that isn't a MMP
val halos = sqlContext.read.parquet("cacheTrees")
val mmp = halos.filter(halos("MMP")===1)

val trunk = mmp.map(mkHalo(_)).
  map(h => (h.TreeRootID, h)).
  groupByKey().
  map({case (k,v) => (k,v.toArray.sortWith(_.Scale > _.Scale))}).
  map({case (k,v) => cleanTrunk(v)})

// Consider host halos > 1.e11 Msun, and add an ejection tag
val hosts = trunk.filter(h => ((h(0).UPId == -1) && (h(0).Mvir > 1.e11))). // Get hosts
  map(h => s"${h(0).Id}  ${h(0).Mvir} ${isEjected(h)}"). // Write in ejected tags
  saveAsTextFile("ejected")

System.exit(0)