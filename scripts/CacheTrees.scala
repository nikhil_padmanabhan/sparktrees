/**
 *
 * Simply cache the trees to disk. This is a little ugly, since the
 * classes have to be defined in multiple places.
 *
 * The correct answer, of course, is to put this into an application, but
 * figuring out how to do this is a different day's work.
 *
 * Created by npadmana on 6/20/15.
 */

import sqlContext.implicits._

case class Halo(Id : Long, // ID
                DescId : Long, // ID of descendant halo
                UPId : Long, // ID of most massive host halo
                TreeRootID : Long, // ID of halo at tree root
                MMP : Int, // ==1, then is most massive progenitor.
                Scale : Float, // scale factor
                Mvir : Float, // Mass
                x : Float,
                y : Float,
                z : Float
                 )


// Simple wrapper
def mkHalo(line: String): Halo = {
  val l = line.trim.split("\\s+")
  new Halo(
    l(1).toLong,
    l(3).toLong,
    l(6).toLong,
    l(29).toLong,
    l(14).toInt,
    l(0).toFloat,
    l(10).toFloat,
    l(17).toFloat,
    l(18).toFloat,
    l(19).toFloat
  )
}

// Strip out comments and other such stuff
// The length removes the first line which is just the number
// of trees in the file. It's a hack.
def isHalo(l: String): Boolean = {
  val l1 = l.trim()
  !(l1.startsWith("#") || (l1.length < 20))
}


val halos = sc.textFile("tree_?_?_?.dat.gz").
  filter(isHalo(_)).
  map(mkHalo(_)).
  toDF.
  saveAsParquetFile("cacheTrees")

System.exit(0)
