# README #

This is a simple demonstration of using Apache Spark to parse the Bolshoi 
halo trees. 

The actual scripts that I run are in the scripts subdirectory. These are
set up to be passed directly to spark-shell -i. We assume you have downloaded
the Bolshoi Consistent Trees and are running these scripts where you
downloaded them.

I have tried to design the scripts so that the same script can be used to 
debug on my laptop and run at NERSC (or any other similar cluster). This is
all a first attempt, and so very little has been done to optimize any of this.


## Description of scripts ##
  
**CacheTrees** :
  I got tired of re-parsing the files over and over 

**SimpleEjectedHalos** :
  Scans through the merger trees; writes out z=0 host halo ID, mass and whether
  they were ever ejected or not. The ejected criterion is simply whether or 
  not the UPid of any halo on the main trunk was != -1. Note that while this is
  a rather simple output, it does actually have the full trunk, so one can do 
  much more with it (eg. halo mass histories etc). ***Note that this does use
  the results of CacheTrees***
 
**EjectedHalos** :
  Does the full problem of determining ejected halos, finding their hosts at 
  the correct redshift, then walking back to z=0 and figuring out who and where 
  their hosts are today. As with the previous case, I do not attempt any 
  optimizations here, and just implement this as a straight series of joins over
  the full merger tree. Of course, this could be made much better by appropriately
  grouping halos, but this is an interesting proof of principle. As with the previous
  case, I restrict to host halos > 1.e11 in mass.
  The output of this is 
```
 <Id>  <Mass> <x> <y> <z> <HostId> <HostMass> <xHost> <yHost> <zHost>
```
  If the halo is not ejected, then the host is the halo itself. 
  ***Note that this does use the results of CacheTrees, but is otherwise
  self-contained. It does not depend on SimpleEjectedHalos***
 
## Results ##

A set of results is in the results directory. These were obtained by running 
CacheTrees and SimpleEjectedHalos/EjectedHalos (there is no dependency between
the last two tasks) at NERSC on the full Bolshoi halo trees untouched. I ran these
on 8 Edison nodes (1 master + 7 slaves), running with 4 cores. The jobs were 
all run interactively.
 
## Notes for running on Edison at NERSC ##

The NERSC webpages give you most of the details, so start there. One issue : by
default, spark seems to run 48 executors on each node, which basically has it 
running out of memory almost instantly.

In spark-env.sh, I set
```
export SPARK_WORKER_MEMORY=32G
export SPARK_WORKER_CORES=4
```
which seemed to fix things. In spark-defaults.conf, I also set
```
spark.driver.memory=8g
spark.executor.memory=8g
```
I'm not sure of the correctness of this, but it seemed to work.

